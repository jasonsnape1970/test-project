resource "random_id" "rg-name" {
  byte_length = 8
  prefix      = var.resource_group_name_prefix
}

resource "azurerm_resource_group" "rg" {
  name     = random_id.rg-name.id
  location = var.resource_group_location
}