terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 3.2"
    }
  }
}

provider "azurerm" {
  features {}
}

# Your code goes here